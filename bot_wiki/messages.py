message_start = '''
{},  я бот @wik_a_bot
я предлагаю статьи из википедии по твоему запросу
просто нажми введи запрос в сообщении и жди ответа и выбирай любую статью
так же ты можешь получить случайную статью нажав на 🎰 😉
приступай, я всегда буду рада помочь
'''

message_help = '''
{}, для поиска статьи можешь просто ввести что тебя интересует или нажми на 🔍
для того чтоб получить случайную статью нажми на 🎰 и я что - то поищу
'''

message_find = '''
{}, введи что ты хочешь искать
'''

message_random = '''
{}, вот что я нашла по твоему запросы,
если хочешь прочесть просто клацни по ней
'''

message_error_get_content = '''
{}, извини не вышло получить контент страницы, но вот ссылка на страницу в Wikipedia
'''