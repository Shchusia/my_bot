import psycopg2
from config import str_connect_to_db
import traceback


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def decorator_db(fn):
    def wrap(*args, **kwargs):
        conn = create_connection()
        try:
            if conn:
                kwargs['conn'] = conn
                data = fn(*args,
                          **kwargs)
                conn.close()
                return data
            return None
        except:
            traceback.print_exc()
            conn.close()
            return None
    return wrap


@decorator_db
def registration(id_user,
                 first_name,
                 last_name,
                 *args,
                 **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_find = '''
        SELECT id_user_in_telegram
        FROM users
        WHERE id_user_in_telegram = {}
    '''.format(id_user)
    cur.execute(select_find)
    row = cur.fetchone()
    if row:
        return True
    select_insert = '''
        INSERT INTO users (id_user_in_telegram, first_name, last_name)
        VALUES ({},'{}','{}');
    '''.format(id_user, first_name,last_name)
    cur.execute(select_insert)
    return True


@decorator_db
def insert_message(id_user,
                   title,
                   url,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    insert_select = '''
        INSERT INTO message (id_message,title,url,date,ref_id_user_in_telegram)
        VALUES (DEFAULT, '{}', '{}', DEFAULT , {})
    '''.format(title.riplace("'", "''"),
               url.riplace("'", "''"),
               id_user)
    cur.execute(insert_select)
    conn.commit()


@decorator_db
def is_exist_text(id_user,
                  text,
                  *args,
                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT title,url
        FROM message
        WHERE ref_id_user_in_telegram = {}
        AND title = '{}'
    '''.format(id_user, text)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        return True, {
            'title': row[0],
            'url': row[1]
        }
    return False, {}
