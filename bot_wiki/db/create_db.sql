CREATE SEQUENCE message_id START 1;


CREATE TABLE users(
  id_user_in_telegram INT PRIMARY KEY,
  first_name VARCHAR(100),
  last_name VARCHAR(100)
);

CREATE TABLE message(
  id_message INT PRIMARY KEY DEFAULT nextval('message_id'),
  title VARCHAR(100),
  url TEXT,
  date VARCHAR(100),
  ref_id_user_in_telegram INT REFERENCES users(id_user_in_telegram)
)
