import telebot as tb
from config import token
from emoji import find,\
    random_machine,\
    sos
from pprint import pprint
from messages import message_help, \
    message_start,\
    message_find,\
    message_random,\
    message_error_get_content

from wik_a import random_page, \
    search,\
    print_content_page

user_markup_default = tb.types.ReplyKeyboardMarkup(False, True, True)
user_markup_default.row(find, random_machine)
user_markup_default.row(sos)

LENGTH_CONTENT = 5000


bot = tb.TeleBot(token)


@bot.message_handler(commands=['start'])
def handler_text(message):
    registration(message.from_user.id,message.chat.first_name,message.chat.last_name)
    bot.send_sticker(message.from_user.id,
                     'CAADAgADQgADGgZFBJvaLJpbpAXhAg')
    bot.send_message(message.from_user.id,
                     message_start.format(message.chat.first_name),
                     reply_markup=user_markup_default)


@bot.message_handler(content_types=['text'])
def handler_text(message):
    if message.text == random_machine:
        title, url = random_page()
        '''insert_message(message.from_user.id,
                       title,
                       url)
        '''
        user_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
        user_markup.row(title)
        user_markup.row(find, random_machine)
        user_markup.row(sos)

        '''
        answer = tb.types.InlineKeyboardButton(r, url=res[r])
        answer.text = r
        answer.url = res[r]

        '''
        bot.send_message(message.from_user.id,
                         message_random.format(message.chat.first_name),
                         reply_markup=user_markup)

    elif message.text == sos:
        bot.send_message(message.from_user.id,
                         message_help.format(message.chat.first_name),
                         reply_markup=user_markup_default)
    elif message.text == find:
        bot.send_message(message.from_user.id,
                         message_find.format(message.chat.first_name),
                         reply_markup=user_markup_default)
    else:
        is_exist, data, = is_exist_text(message.from_user.id,
                                        message.text)
        if is_exist:
            content = print_content_page(data['url'], data['title'])
            if content:
                # вернуть контент
                content = ((content.replace('====', '*')).replace('===', '_')).replace('==', '```')
                if len(content) < LENGTH_CONTENT:
                    bot.send_message(message.from_user.id, 'а вот и сама статья 😌\n{}\n[{}](URL)'.format(content, data['url']),
                                     parse_mode='Markdown',
                                     reply_markup=user_markup_default,
                                     disable_web_page_preview=True)
                else:
                    i = 0
                    bot.send_message(message.from_user.id,
                                     'а вот и сама статья 😌\n[{}](URL)'.format(data['url']),
                                     disable_web_page_preview=True)
                    while True:
                        bot.send_message(message.from_user.id,
                                         '{}'.format(content[i * LENGTH_CONTENT:(i + 1) * LENGTH_CONTENT]),
                                         parse_mode='Markdown')

                        if len(content) <= (i + 1) * LENGTH_CONTENT:
                            break
                        i += 1
                    bot.send_message(message.from_user.id, 'конец😉✌', reply_markup=user_markup_default)

            else:
                keyboard = tb.types.InlineKeyboardMarkup()
                url_button = tb.types.InlineKeyboardButton(text=data['title'], url=data['url'])
                keyboard.add(url_button)
                bot.send_sticker(message.from_user.id,
                                 'CAADAgADigADGgZFBHF3_01Pcn2FAg')

                bot.send_message(message.from_user.id,
                                 message_error_get_content.format(message.chat.first_name),
                                 reply_markup=keyboard)

        else:
            res = search(message.text)
            if len(res) > 0:
                user_markup = tb.types.ReplyKeyboardMarkup(True, True)
                for r in res.keys():
                    user_markup.row(r)
                    insert_message(message.from_user.id,
                                   r,
                                   res[r])
                bot.send_message(message.from_user.id,
                                 "Вот что я нашла по твоему запрос '{}', {} 😌".format(message.text,
                                                                                       message.chat.first_name),
                                 reply_markup=user_markup)
            else:
                answer = 'По твоему запросу, {}, я ничего не нашла'.format(message.chat.first_name)
                bot.send_message(message.chat.id, '<b>{}</b>😔'.format(answer),
                                 parse_mode='HTML',
                                 disable_web_page_preview=False,
                                 reply_markup=user_markup_default)

