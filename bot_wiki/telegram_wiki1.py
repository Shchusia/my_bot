
import telebot as tb
import datetime

from config_telegram import token,\
    help
from db_work import insert_message,\
    is_old_select
from wik_a import search,\
    random_page,\
    print_content_page
import traceback

bot = tb.TeleBot(token)


@bot.message_handler(commands=['start'])
def handler_text(message):
    user_merkup = tb.types.ReplyKeyboardMarkup(True,True)  # еще один True сделает ее одноразовой
    user_merkup.row('/start', '/help')
    user_merkup.row('/random_page')
    bot.send_message(message.from_user.id, 'привет {} 😋'.format(message.chat.first_name), reply_markup=user_merkup)


@bot.message_handler(commands=['help'])
def handler_text(message):
    bot.send_message(message.chat.id,
                     help.format(message.chat.first_name))


@bot.message_handler(commands=['random_page'])
def handler_text(message):
    answer = random_page()
    #keyboard = tb.types.InlineKeyboardMarkup()
    title = list(answer.keys())[0]
    #url_button = tb.types.InlineKeyboardButton(text=title, url=answer[title])
    #keyboard.add(url_button)
    user_merkup = tb.types.ReplyKeyboardMarkup(True, True)
    user_merkup.row(title)

    bot.send_message(message.chat.id, "Почитай может тебя заинтересует 😉", reply_markup=user_merkup)
    insert_message(message.chat.id,
                   message.chat.first_name,
                   message.chat.last_name,
                   message.text,
                   '{} - {}'.format(title,answer[title]),
                   datetime.datetime.now())



@bot.message_handler(content_types=['text'])
def handler_text(message):
    def answer_for_db(answer):
        res = ''
        for r in answer.keys():
            res += str(r) + ' - ' + str(answer[r]) + '\n'
        return res

    def create_answ():
        res = search(message.text)
        if len(res.keys()) > 0:
            answer = ''
            for r in res.keys():
                '''
                answer += str(r) + \
                          ' - ' +\
                          str(res[r]) + '\n'
                '''
                # answer += '<a href="{}">{}</a>\n'.format(res[r], r)
                answer = tb.types.InlineKeyboardButton(r, url=res[r])
                answer.text = r
                answer.url = res[r]

        else:
            answer = 'On the wiki I did not find the article '

        return answer

    def up_text(content):
        content.replace('')

    res = is_old_select(message.chat.id,message.text)
    # print(res)
    if res == None:
        res = search(message.text)
        # print(message.text)
        if len(res) > 0:
            answer = answer_for_db(res)
            # keyboard = tb.types.InlineKeyboardMarkup()
            user_merkup = tb.types.ReplyKeyboardMarkup(True, True)
            for r in res.keys():
                user_merkup.row(r)
                # url_button = tb.types.InlineKeyboardButton(text=r, url=res[r])
                # keyboard.add(url_button)
            bot.send_message(message.from_user.id, "Вот что я нашла по твоему запрос '{}', {} 😌".format(message.text, message.chat.first_name), reply_markup=user_merkup)
            # bot.send_message(message.chat.id, "Вот что я нашла по твоему запрос '{}', {} 😌".format(message.text, message.chat.first_name), reply_markup=keyboard)
        else:
            answer = 'По твоему запросу, {}, я ничего не нашла'.format(message.chat.first_name)
            bot.send_message(message.chat.id, '<b>{}</b>😔'.format(answer),
                             parse_mode='HTML',
                             disable_web_page_preview=False)

        insert_message(message.chat.id,
                       message.chat.first_name,
                       message.chat.last_name,
                       message.text,
                       answer,
                       datetime.datetime.now())
    else:
        # получаем страницу

        content = print_content_page(res,message.text)
        if res == None:
            keyboard = tb.types.InlineKeyboardMarkup()
            url_button = tb.types.InlineKeyboardButton(text=message.text, url=res)
            keyboard.add(url_button)

            bot.send_message(message.from_user.id,
                             "извини, {}, но не вышло получить контент страницы вот ссылка на статью ".format
                             (message.text,  message.chat.first_name),reply_markup=keyboard)
        else:
            content = ((content.replace('====', '*')).replace('===', '_')).replace('==','```')
            try:
                '''keyboard = tb.types.InlineKeyboardMarkup()
                url_button = tb.types.InlineKeyboardButton(text=str(message.text), url=str(res))
                keyboard.add(url_button)
                bot.send_message(message.from_user.id,
                                 '{},вот ссылка на статью'.format(message.chat.first_name),
                                 reply_markup=keyboard)'''

                user_merkup = tb.types.ReplyKeyboardMarkup(True, True)
                user_merkup.row('/start', '/help')
                user_merkup.row('/random_page')

                bot.send_message(message.from_user.id,'а вот и сама статья 😌\n{}\n[{}](URL)'.format(content,res),
                                parse_mode='Markdown')
                                #reply_markup=user_merkup)
            except:
                traceback.print_exc()
            # bot.send_message(message.from_user.id,reply_markup=user_merkup)


@bot.message_handler(content_types=['document'])
def handler_text(message):
    answer = "я не умею еще работать с документами"
    bot.send_message(message.chat.id, answer)
    insert_message(message.chat.id,
                   message.chat.first_name,
                   message.chat.last_name,
                   message.text,
                   answer,
                   datetime.datetime.now())


@bot.message_handler(content_types=['audio'])
def handler_text(message):
    answer = "я не умею еще работать с audio"
    bot.send_message(message.chat.id, answer)
    insert_message(message.chat.id,
                   message.chat.first_name,
                   message.chat.last_name,
                   message.text,
                   answer,
                   datetime.datetime.now())


@bot.message_handler(content_types=['photo'])
def handler_text(message):
    answer = "я не умею еще работать с картинками"
    bot.send_message(message.chat.id, answer)
    insert_message(message.chat.id,
                   message.chat.first_name,
                   message.chat.last_name,
                   message.text,
                   answer,
                   datetime.datetime.now())


@bot.message_handler(content_types=['sticker'])
def handler_text(message):
    answer = "я не умею еще работать с стикерами"
    bot.send_message(message.chat.id, answer)
    insert_message(message.chat.id,
                   message.chat.first_name,
                   message.chat.last_name,
                   message.text,
                   answer,
                   datetime.datetime.now())


bot.polling(none_stop=True, interval=0)
