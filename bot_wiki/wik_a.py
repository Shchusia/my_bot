import wikipedia

wikipedia.set_lang("ru")


def search(user_query):
    '''title_article = wikipedia.search(user_query)
    res = {}
    for ta in title_article:
        article = wikipedia.page(str(ta))
        res[ta] = article.url
    return res'''
    r = wikipedia.search(user_query, suggestion=False)
    # print(r)
    res = {}
    for rr in r:
        try:
            t = wikipedia.page(rr.replace(' ', '_'))
            res[rr] = t.url
        except:
            pass
    return res


def random_page():
    while True:
        try:
            p = wikipedia.random()
            t = wikipedia.page(p.replace(' ', '_'))
            return str(p), str(t.url)
        except:
            pass


def print_content_page(url, message):
    # print(r)
    try:
        t = wikipedia.page(message.replace(' ', '_'))
        # print(str(t.content).find('== (([a-zA-ZА-Яа-я])+ )+=='))
        return t.content
    except:
        return None
