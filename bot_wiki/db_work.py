import psycopg2
from config_telegram import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print ("I am unable to connect to the database")
    return conn


def is_exist_user(id_user):
    select = '''
        SELECT id_user_in_telegram
        FROM users
        WHERE id_user_in_telegram = {}
    '''.format(id_user)
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute(select)
        res = cur.fetchall()

        cur.close()
        conn.close()
        if len(res) == 0:
            return False
        else:
            return True
    return None


def insert_message(id_user, name, surname, message,answer,date_sel):
    res = is_exist_user(id_user)
    if res is not None:
        conn = create_connection()
        if res is True:
            select = """
                INSERT INTO message (id_message, message, answer, "date", ref_id_user_in_telegram)
                VALUES (DEFAULT, '{}', '{}', '{}', {})
            """.format(message,answer,date_sel,id_user)
        else:
            select = """
                INSERT INTO users (id_user_in_telegram, first_name, last_name)
                VALUES ({},'{}','{}');
                INSERT INTO message (id_message, message, answer, "date", ref_id_user_in_telegram)
                VALUES (DEFAULT, '{}', '{}', '{}', {});
            """.format(id_user,
                       name,
                       surname,
                       message,
                       answer,
                       date_sel,
                       id_user)
        cur = conn.cursor()
        cur.execute(select)
        conn.commit()
        cur.close()
        conn.close()

    return {
        'code':-1,
        'message': 'error connect to db'
    }


def is_old_select(id_user, message):
    select = """
        SELECT answer
        FROM message
        WHERE id_message = (SELECT max(id_message)
                            FROM message
                            WHERE ref_id_user_in_telegram = {})
    """.format(id_user)
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute(select)
        answer = cur.fetchall()
        # print(1)
        # len(answer)
        if len(answer)>0:
           answer =answer[0][0]
           if answer.find(message) > 1:
                #print(1)
                arr = answer.split('\n')
                #print(arr)
                for r in arr:
                    if r.find(message) != -1:
                        return r.split('-')[1].replace(' ', ' ')
                return None
           else:
               return None
        else:
            return None
    return None


