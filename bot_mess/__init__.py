# -*- coding: utf-8 -*-
import telebot as tb
import traceback
from pprint import pprint
from config import token
from messages_answer import help, \
    new_group as ng,\
    start,\
    help_start,\
    help_group
from data_base.db_logger import get_last_step_user,\
    to_logger_step_user
from data_base.db_groups_message import get_group_user,\
    get_groups_user,\
    create_group_user,\
    delete_groups
from data_base.redis import set_last_step_user,\
    get_last_step_ser,\
    is_exist_user,\
    delete_user
from data_base.db_message import get_message,\
    get_messages,\
    set_forward_message,\
    set_message,\
    delete_message,\
    set_forward_photo,\
    set_photo
from data_base.__init__ import registration_user

bot = tb.TeleBot(token)


@bot.message_handler(commands=['start'])
def handler_text(message):
    registration_user(message.chat.first_name,
                      message.chat.last_name,
                      message.from_user.id)
    to_logger_step_user(message.from_user.id,
                        0,
                        message.text)
    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
    user_markup.row('➕', '📃', '✖')
    user_markup.row('❓')
    bot.send_message(message.from_user.id, start.format(message.chat.first_name), reply_markup=user_markup)


@bot.message_handler(commands=['help'])
def handler_text(message):
    bot.send_message(message.from_user.id, help)


@bot.message_handler(commands=['create_group'])
def handler_text(message):
    to_logger_step_user(message.from_user.id,
                        1,
                        message.text)
    bot.send_message(message.from_user.id,
                     ng.format(message.chat.first_name))


@bot.message_handler(commands=['get_groups'])
def handler_text(message):
    group = get_groups_user(message.from_user.id)
    to_logger_step_user(message.from_user.id,
                        2,
                        'get_groups')
    if group['code'] > 0:
        res = group['data']
        if len(res) > 0:
            user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
            for r in res:
                user_markup.row(r[1])
            user_markup.row('без групп')
            bot.send_message(message.from_user.id,
                             'твои группы {}'.format(message.chat.first_name),
                             reply_markup=user_markup)
        elif len(res) == 0:
            answer = 'список групп сообщений пуст'
    else:
       answer = group['message']
    bot.send_message(message.from_user.id,
                     answer)


@bot.message_handler(content_types=['text'])
def handler_text(message):
    # pprint(message)
    try:
        step = get_last_step_user(message.from_user.id)['data']
        # print('___')
        # print(step)
        if message.text == '🔙':
            if not is_exist_user(message.from_user.id):
                to_logger_step_user(message.from_user.id,
                                    0,
                                    '/start')
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('➕', '📃', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id, 'вы вернулись к главной', reply_markup=user_markup)
            else:
                delete_user(message.from_user.id)
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('🔙', '👀', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id,
                                 'вы вернулись к группе "{}"'.format(step[1]),
                                 reply_markup=user_markup)

        elif step[0] == 0:
            if step[1] != '✖':
                if message.text == '➕':
                    to_logger_step_user(message.from_user.id,
                                        1,
                                        message.text)
                    bot.send_message(message.from_user.id,
                                     ng.format(message.chat.first_name))
                elif message.text == '📃':
                    group = get_groups_user(message.from_user.id)
                    to_logger_step_user(message.from_user.id,
                                        2,
                                        'get_groups')
                    if group['code'] > 0:
                        res = group['data']
                        if len(res) > 0:
                            user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                            for r in res:
                                user_markup.row(r[1])
                            user_markup.row('без групп')
                            user_markup.row('🔙')
                            bot.send_message(message.from_user.id,
                                             'твои группы {}'.format(message.chat.first_name),
                                             reply_markup=user_markup)
                        elif len(res) == 0:
                            answer = 'список групп сообщений пуст'
                            bot.send_message(message.from_user.id,
                                             answer)
                    else:
                        answer = group['message']
                        bot.send_message(message.from_user.id,
                                         answer)
                elif message.text == '❓':
                    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                    user_markup.row('➕', '📃', '✖')
                    user_markup.row('❓')
                    bot.send_message(message.from_user.id,
                                     help_start.format(message.from_user.first_name),
                                     reply_markup=user_markup)
                elif message.text == '✖':
                    group = get_groups_user(message.from_user.id)
                    to_logger_step_user(message.from_user.id,
                                        0,
                                        '✖')
                    if group['code'] > 0:
                        res = group['data']
                        if len(res) > 0:
                            user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                            for r in res:
                                user_markup.row(r[1])
                            user_markup.row('без групп')
                            user_markup.row('🔙')
                            bot.send_message(message.from_user.id,
                                             'твои группы {} выбери какую удалить'.format(message.chat.first_name),
                                             reply_markup=user_markup)
                        elif len(res) == 0:
                            answer = 'список групп сообщений пуст'
                            bot.send_message(message.from_user.id,
                                             answer)
                    else:
                        answer = group['message']
                        bot.send_message(message.from_user.id,
                                         answer)
                else:
                    if message.forward_from is not None:
                        # пересылается
                        data = set_forward_message(message.from_user.id,
                                                   message.text,
                                                   message.forward_from.first_name,
                                                   message.forward_from.last_name,
                                                   message.forward_from.id)
                    else:
                        data = set_message(message.from_user.id,
                                           message.text)
                    if data['code'] > 0:
                        answer = 'сообщение добавлено'
                    else:
                        answer = data['message']
                    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                    user_markup.row('➕', '📃', '✖')
                    user_markup.row('❓')
                    bot.send_message(message.from_user.id, answer, reply_markup=user_markup)
            elif step[1] == '✖':
                ans = delete_groups(message.from_user.id,  message.text)
                to_logger_step_user(message.from_user.id,
                                    0,
                                    '/start')
                if ans['code'] > 0:
                    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                    user_markup.row('➕',  '📃', '✖')
                    user_markup.row('❓')
                    bot.send_message(message.from_user.id, 'группа удалена', reply_markup=user_markup)
                else:
                    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                    user_markup.row('➕',  '📃', '✖')
                    user_markup.row('❓')
                    bot.send_message(message.from_user.id, 'группа не удалена', reply_markup=user_markup)
            else:
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('➕', '📃', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id, 'что - то пошло не так', reply_markup=user_markup)
        elif step[0] == 1:  # создать новую группу
            ans = create_group_user(message.from_user.id, message.text)
            bot.send_message(message.from_user.id,
                             ans['message'])
            to_logger_step_user(message.from_user.id,
                               2,
                               message.text)
            if ans['code'] > 0:  # перейти в группу
                bot.send_message(message.from_user.id,
                                 'вы перешли в группу "{}"'.format(message.text))
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('🔙', '👀', '✖')
                user_markup.row('❓')
                # user_markup.row('/help')
                bot.send_message(message.from_user.id, 'у вас пока нет сообщений но все пересланные сообщения будут попадать сюда пока вы не выберете другую группу'.format(message.text),
                                 reply_markup=user_markup)
        elif step[0] == 2:
            if message.text == '👀':
                # показать сообщения залогировать что мы смотри
                set_last_step_user(message.from_user.id, 1)
                messages = get_messages(message.from_user.id)['data']
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                for r in messages:
                    user_markup.row(r)
                user_markup.row('🔙')
                bot.send_message(message.from_user.id,
                                 'твои сообщения {}'.format(message.chat.first_name),
                                 reply_markup=user_markup)
            elif message.text == '✖':
                set_last_step_user(message.from_user.id, 2)
                messages = get_messages(message.from_user.id)['data']
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                for r in messages:
                    user_markup.row(r)
                user_markup.row('🔙')
                bot.send_message(message.from_user.id,
                                 'твои сообщения {}'.format(message.chat.first_name),
                                 reply_markup=user_markup)
                # показать сообщения из группы для удаления залогировать что мы удаляем
                pass
            elif message.text == '❓':
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('🔙', '👀', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id,
                                 help_group.format(message.from_user.first_name),
                                 reply_markup=user_markup)
            elif step[1] == 'get_groups':
                to_logger_step_user(message.from_user.id,
                                    2,
                                    message.text)

                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('🔙', '👀', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id,
                                 'вы перешли в группу "{}"'.format(message.text),
                                 reply_markup=user_markup)
            elif message.text == '🔙':
                # вернуться в главное меню
                set_last_step_user(message.from_user.id, 0)
                to_logger_step_user(message.from_user.id,
                                    0,
                                    message.text)
                user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                user_markup.row('➕', '📃', '✖')
                user_markup.row('❓')
                bot.send_message(message.from_user.id, start.format(message.chat.first_name), reply_markup=user_markup)
            else:
                if is_exist_user(message.from_user.id):
                    step = get_last_step_ser(message.chat.id)
                    delete_user(message.chat.id)
                    if step == 1:
                        id_mes = (message.text).split(' ')[0]
                        data = get_message(message.chat.id, id_mes)
                        if data['code'] > 0:
                            if data['data']['is_message']:
                                answer = 'сообщение от {}\nотправлено: {}\n\n{}'.format(data['data']['from'],
                                                                                      data['data']['date'],
                                                                                      data['data']['text'])
                            else:
                                answer = 'сообщение от {}\nотправлено: {}\n\n'.format(data['data']['from'],
                                                                                        data['data']['date'])
                        else:
                            answer = data['message']
                        user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                        user_markup.row('🔙', '👀', '✖')
                        user_markup.row('❓')
                        bot.send_message(message.from_user.id, answer,
                                         reply_markup=user_markup)
                        if data['code'] > 0:
                            if not data['data']['is_message']:
                                bot.send_photo(message.from_user.id, data['data']['file_id'])
                    else:
                        id_mes = (message.text).split(' ')[0]
                        res = delete_message(message.chat.id, id_mes)
                        user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                        user_markup.row('🔙', '👀', '✖')
                        user_markup.row('❓')
                        bot.send_message(message.from_user.id, res['message'],
                                         reply_markup=user_markup)
                elif get_last_step_user(message.from_user.id)['data'][1] == 'get_groups':
                    res = get_group_user(message.from_user.id, message.text)
                    if res['code'] > 0:
                        to_logger_step_user(message.from_user.id,
                                            2,
                                            message.text)
                        bot.send_message(message.from_user.id,
                                         'вы перешли в группу "{}"'.format(message.text))
                        user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                        user_markup.row('🔙', '👀', '✖')
                        user_markup.row('❓')
                        bot.send_message(message.from_user.id, '',
                                         reply_markup=user_markup)
                    else:
                        user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                        user_markup.row('🔙', '👀', '✖')
                        user_markup.row('❓')
                        bot.send_message(message.from_user.id, res['message'],
                                         reply_markup=user_markup)


                else:
                    if message.forward_from is not None:
                        # пересылается
                        data = set_forward_message(message.from_user.id,
                                                   message.text,
                                                   message.forward_from.first_name,
                                                   message.forward_from.last_name,
                                                   message.forward_from.id)
                    else:
                        data = set_message(message.from_user.id,
                                           message.text)
                    if data['code'] > 0:
                        answer = 'сообщение добавлено'
                    else:
                        answer = data['message']
                    user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
                    user_markup.row('🔙', '👀', '✖')
                    user_markup.row('❓')
                    bot.send_message(message.from_user.id, answer,
                                     reply_markup=user_markup)
                    # запись message
                # проверить предыдущий шаг в логах
                # если мы смотрим то показать сообщение если нет то удаляем
                pass
        else:
            to_logger_step_user(message.from_user.id,
                                0,
                                '/start')
            user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
            user_markup.row('➕', '📃', '✖')
            user_markup.row('❓')
            bot.send_message(message.from_user.id, 'упс 😅, хз что делать', reply_markup=user_markup)
    except:
        traceback.print_exc()

        to_logger_step_user(message.from_user.id,
                            0,
                            '/start')
        user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
        user_markup.row('➕', '📃', '✖')
        user_markup.row('❓')
        bot.send_message(message.from_user.id, 'упс 😔, что - то пошло не так', reply_markup=user_markup)


@bot.message_handler(content_types=['photo'])
def handler_text(message):
    if message.forward_from is not None:
        # пересылается
        data = set_forward_photo(message.from_user.id,
                                 message.photo[0].file_id,
                                 message.forward_from.first_name,
                                 message.forward_from.last_name,
                                 message.forward_from.id)
    else:
        data = set_photo(message.from_user.id,
                         message.photo[0].file_id)
    if data['code'] > 0:
        answer = 'сообщение добавлено'
    else:
        answer = data['message']
    bot.send_message(message.from_user.id, answer)


@bot.message_handler(content_types=['document', 'audio', 'sticker','video'])
def handler_text(message):
    bot.send_message(message.from_user.id, '{}, извини но  я не работаю с таким типом данных 😌'.format(message.from_user.first_name))
#@bot.message_handler(content_types=['audio'])

while True:
    try:
        bot.polling(none_stop=True, interval=1)
    except:
        pass