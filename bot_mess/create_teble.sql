CREATE SEQUENCE users_id START 1;
CREATE SEQUENCE contacts_id START 1;
CREATE SEQUENCE groups_id START 1;
CREATE SEQUENCE message_text_id START 1;

CREATE SEQUENCE logger_step_user_id START 1;
CREATE TABLE users(
  id_user INT PRIMARY KEY DEFAULT nextval('users_id'),
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  chat_id VARCHAR(20),
  created_on date default now()
);

CREATE TABLE contacts(
  id_contacts INT PRIMARY KEY DEFAULT nextval('contacts_id'),
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  chat_id VARCHAR(20) ,
  created_on date default now(),
  ref_id_user INT REFERENCES users(id_user)
);

CREATE TABLE groups(
  id_group INT PRIMARY KEY DEFAULT nextval('groups_id'),
  name_group VARCHAR(100),
  ref_id_users INT REFERENCES users(id_user),
  created_on DATE DEFAULT now()
);

CREATE TABLE message_text(
  id_message_text INT PRIMARY KEY DEFAULT nextval('message_text_id'),
  text TEXT,
  is_user BOOL DEFAULT TRUE,
  created_on VARCHAR(20),
  ref_id_users INT REFERENCES users(id_user),
  ref_id_contacts INT REFERENCES contacts(id_contacts),
  ref_id_groups INT REFERENCES groups(id_group),
  is_message BOOL DEFAULT TRUE,
  file_id VARCHAR(100)
);

CREATE TABLE logger_step_user(
  id_logger_step_user INT PRIMARY KEY DEFAULT nextval('logger_step_user_id'),
  value_step INT,
  ref_id_user INT REFERENCES users(id_user),
  message TEXT,
  created_on DATE DEFAULT now()
);

INSERT INTO groups (id_group, name_group, created_on) VALUES (0, 'без групп', now());