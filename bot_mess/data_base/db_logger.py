import psycopg2
from config import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback
from data_base.answer_db import error_connect,\
    error_select,\
    step_to_log_added
from data_base.__init__ import create_connection, \
    get_id_user_by_chat_id


def to_logger_step_user(chat_id, value, text):
    '''

    :param chat_id:
    :param value:
     № 1 создать группу
     № 2 выбор групп сообщений


    :return:
    '''
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                INSERT INTO logger_step_user (id_logger_step_user,value_step,ref_id_user,message )
                VALUES (DEFAULT, {}, {}, '{}')
            '''.format(value, get_id_user_by_chat_id(chat_id, cur), text)
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1,
                'message': step_to_log_added
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def get_last_step_user(chat_id):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                SELECT value_step, message
                FROM logger_step_user
                WHERE id_logger_step_user = (
                  SELECT max(id_logger_step_user)
                  FROM logger_step_user
                  WHERE ref_id_user = {})
            '''.format(get_id_user_by_chat_id(chat_id, cur))
            cur.execute(select)
            step = cur.fetchone()
            cur.close()
            conn.close()
            return {
                'code': 1,
                'message': step_to_log_added,
                'data': step
            }
        except:
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }