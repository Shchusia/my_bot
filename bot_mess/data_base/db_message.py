import psycopg2
from config import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback
from data_base.answer_db import error_connect,\
    error_select,\
    step_to_log_added,\
    error_group,\
    group_created

from data_base.__init__ import create_connection, \
    get_id_user_by_chat_id
from data_base.db_logger import get_last_step_user


def get_messages(chat_id):
    def get_contacts(id_contact, cur):
        select = '''
            SELECT c.first_name, c.last_name
            FROM contacts c
            WHERE c.id_contacts = {}
        '''.format(id_contact)
        cur.execute(select)
        c = cur.fetchone()
        var = c[0]
        if c[1] != 'None':
            var += ' ' + c[1]
        return var

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            step = get_last_step_user(chat_id)['data'][1]
            select = '''
                SELECT mt.id_message_text, mt.created_on, mt.is_user, mt.ref_id_contacts
                FROM message_text mt, groups g
                WHERE mt.ref_id_users = {}
                AND mt.ref_id_groups = g.id_group
                AND g.name_group = '{}'
            '''.format(id_user, step)
            cur.execute(select)
            rows = cur.fetchall()
            answer = []
            for r in rows:
                if r[2]:
                    answer.append('{} {} {}'.format(r[0], 'вы', r[1]))
                else:

                    answer.append('{} {} {}'.format(r[0], get_contacts(r[3], cur), r[1]))
            return {
                'code': 1,
                'data': answer
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def get_message(chat_id, id_message):
    def get_contacts(id_contact, cur):
        select = '''
            SELECT c.first_name, c.last_name
            FROM contacts c
            WHERE c.id_contacts = {}
        '''.format(id_contact)
        cur.execute(select)
        c = cur.fetchone()
        var = c[0]
        if c[1] != 'None':
            var += ' ' + c[1]
        return var

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            step = get_last_step_user(chat_id)['data'][1]
            select = '''
                SELECT mt.id_message_text, mt.created_on, mt.is_user, mt.ref_id_contacts,mt.text,mt.is_message, mt.file_id
                FROM message_text mt, groups g
                WHERE mt.ref_id_users = {}
                AND mt.ref_id_groups = g.id_group
                AND g.name_group = '{}'
               and mt.id_message_text = {}
            '''.format(id_user, step, id_message)
            # print(select)
            cur.execute(select)
            r = cur.fetchone()
            answer = ''
            if r[2]:
                from_m = 'вас'
            else:
                from_m = get_contacts(r[3], cur)
            return {
                'code': 1,
                'data': {
                    'from': from_m,
                    'date': r[1],
                    'text': r[4],
                    'id_message': r[0],
                    'is_message': r[5],
                    'file_id': r[6]

                }
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def delete_message(chat_id, id_message):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''DELETE FROM message_text WHERE id_message_text = {} AND ref_id_users = {}'''.format(id_message, id_user)
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1,
                'message': 'сообщение удалено'
            }
        except:
            return {
                'code': -1,
                'message': "соообщение не удалено"
            }
    return {
        'code': -1,
        'message': error_connect
    }


def set_message(chat_id, text):
    def get_id_group(name, cur):
        select = '''
        SELECT id_group
        FROM groups
        WHERE name_group = '{}'
        '''.format(name)
        cur.execute(select)
        try:
            return cur.fetchone()[0]
        except:
            return 0

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            step = get_last_step_user(chat_id)['data'][1]
            id_user = get_id_user_by_chat_id(chat_id, cur)

            select = '''
                INSERT INTO message_text (id_message_text, text, is_user, created_on, ref_id_users, ref_id_groups)
                VALUES (DEFAULT, '{}', TRUE, now(), {},{})
            '''.format(text, id_user, get_id_group(step, cur))
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def set_forward_message(chat_id, text,first_name, last_name, chat_id_con,):
    def get_id_contacts(cur):
        select_insert = '''
            INSERT INTO contacts (first_name, last_name, chat_id, created_on, ref_id_user)
            VALUES ('{}', '{}', '{}', now(), {}) Returning id_contacts
        '''.format(first_name, last_name, chat_id_con, get_id_user_by_chat_id(chat_id, cur))
        select_id = '''
          SELECT id_contacts
          FROM contacts
          WHERE chat_id  = '{}'
        '''.format(chat_id_con)
        cur.execute(select_id)
        row = cur.fetchone()
        if row:
            return row[0]
        else:
            cur.execute(select_insert)
            return cur.fetchone()[0]

    def get_id_group(name, cur):
        select = '''
           SELECT id_group
           FROM groups
           WHERE name_group = '{}'
           '''.format(name)
        cur.execute(select)
        try:
            return cur.fetchone()[0]
        except:
            return 0

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            step = get_last_step_user(chat_id)['data'][1]
            id_user = get_id_user_by_chat_id(chat_id, cur)
            id_con = get_id_contacts(cur)
            conn.commit()

            select = '''
                   INSERT INTO message_text (id_message_text, text, is_user, created_on, ref_id_users, ref_id_groups,ref_id_contacts)
                   VALUES (DEFAULT, '{}', FALSE , now(), {},{},{})
               '''.format(text, id_user, get_id_group(step, cur), id_con)
            # print(select)
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def set_photo(chat_id, file_id):
    def get_id_group(name, cur):
        select = '''
        SELECT id_group
        FROM groups
        WHERE name_group = '{}'
        '''.format(name)
        cur.execute(select)
        try:
            return cur.fetchone()[0]
        except:
            return 0

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            step = get_last_step_user(chat_id)['data'][1]
            id_user = get_id_user_by_chat_id(chat_id, cur)

            select = '''
                INSERT INTO message_text (id_message_text, file_id, is_user, created_on, ref_id_users, ref_id_groups, is_message)
                VALUES (DEFAULT, '{}', TRUE, now(), {},{}, FALSE )
            '''.format(file_id, id_user, get_id_group(step, cur))
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def set_forward_photo(chat_id, file_id,first_name, last_name, chat_id_con,):
    def get_id_contacts(cur):
        select_insert = '''
            INSERT INTO contacts (first_name, last_name, chat_id, created_on, ref_id_user)
            VALUES ('{}', '{}', '{}', now(), {}) Returning id_contacts
        '''.format(first_name, last_name, chat_id_con, get_id_user_by_chat_id(chat_id, cur))
        select_id = '''
          SELECT id_contacts
          FROM contacts
          WHERE chat_id  = '{}'
        '''.format(chat_id_con)
        cur.execute(select_id)
        row = cur.fetchone()
        if row:
            return row[0]
        else:
            cur.execute(select_insert)
            return cur.fetchone()[0]

    def get_id_group(name, cur):
        select = '''
           SELECT id_group
           FROM groups
           WHERE name_group = '{}'
           '''.format(name)
        cur.execute(select)
        try:
            return cur.fetchone()[0]
        except:
            return 0

    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            step = get_last_step_user(chat_id)['data'][1]
            id_user = get_id_user_by_chat_id(chat_id, cur)
            id_con = get_id_contacts(cur)
            conn.commit()

            select = '''
                   INSERT INTO message_text (id_message_text, file_id, is_user, created_on, ref_id_users, ref_id_groups,ref_id_contacts,is_message)
                   VALUES (DEFAULT, '{}', FALSE , now(), {},{},{}, FALSE )
               '''.format(file_id, id_user, get_id_group(step, cur), id_con)
            # print(select)
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1
            }
        except:
            traceback.print_exc()
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }
