import redis
import traceback
import datetime

r = redis.StrictRedis(host='localhost', port=6379, db=5)


def set_last_step_user(chat_id, step):
    r.set(chat_id, step)


def get_last_step_ser(chat_id):
    try:
        return int(r.get(chat_id).decode('utf-8'))
    except KeyError:
        return 0


def is_exist_user(chat_id):
    try:
        if not r.get(chat_id) is None:
            return True
        return False
    except:
        return False


def delete_user(chat_id):
    try:
        r.delete(chat_id)
    except:
        pass