import psycopg2
from config import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback
from data_base.answer_db import error_connect,\
    error_select,\
    step_to_log_added,\
    error_group,\
    group_created

from data_base.__init__ import create_connection, \
    get_id_user_by_chat_id


def check_exist_group(id_user, name_group, cursor):
    try:
        select = '''
            SELECT id_group
            FROM groups
            WHERE name_group = '{}'
            AND ref_id_users = {}
        '''.format(name_group, id_user)
        cursor.execute(select)
        n = cursor.fetchone()
        if n:
            return True
        return False
    except:
        return False


def get_group_user(chat_id, name_group):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            if not check_exist_group(id_user, name_group, cur):
                select = '''
                    SELECT id_group
                    FROM groups
                    WHERE name_group = '{}'
                    AND ref_id_users = {}
                '''.format(name_group, id_user)
                cur.execute(select)
                id_group = cur.fetchone()[0]
                cur.close()
                conn.close()
                return {
                    'code': 1,
                    'message': '',
                    'data': id_group
                }
            else:
                return {
                    'code': -2,
                    'message': error_group
                }
        except:
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def create_group_user(chat_id, name_group):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            if not check_exist_group(id_user, name_group, cur):
                select = '''
                        INSERT INTO groups (id_group, name_group, ref_id_users, created_on )
                        VALUES (DEFAULT , '{}', {}, DEFAULT )
                    '''.format(name_group, id_user)
                cur.execute(select)
                conn.commit()
                cur.close()
                conn.close()
                return {
                    'code': 1,
                    'message': group_created
                }
            else:
                return {
                    'code': -2,
                    'message': error_group
                }
        except:
            return {
                'code': -1,
                'message': error_select
            }
    return {
        'code': -1,
        'message': error_connect
    }


def get_groups_user(chat_id):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                    SELECT id_group, name_group
                    FROM groups
                    WHERE ref_id_users = {}
                '''.format(id_user)
            cur.execute(select)
            ans = cur.fetchall()
            cur.close()
            conn.close()
            return {
                'code': 1,
                'message': '',
                'data': [[a[0], a[1]] for a in ans]
                }

        except:
            return {
                'code': -1,
                'message': error_select,
                'data': []
            }
    return {
        'code': -1,
        'message': error_connect,
        'data': []
    }


def delete_groups(chat_id, name_group):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id,cur)
            select_first = '''
                DELETE FROM message_text WHERE ref_id_groups = (SELECT id_group
                                                FROM groups
                                                WHERE name_group = '{}'
                                                AND ref_id_users = {});
            '''.format(name_group, id_user)
            select_second = '''
                DELETE FROM groups WHERE name_group = '{}' AND ref_id_users = {};
            '''.format(name_group, id_user)
            cur.execute(select_first)
            conn.commit()
            if name_group != 'без групп':
                cur.execute(select_second)
                conn.commit()
            else:
                select_first = '''
                                DELETE FROM message_text WHERE ref_id_groups = (SELECT id_group
                                                                FROM groups
                                                                WHERE name_group = '{}')
                                            AND ref_id_users = {};
                            '''.format(name_group, id_user)
                cur.execute(select_first)
                conn.commit()
            cur.close()
            conn.close()
            return {
                'code': 1,
                'message': 'rem'
            }
        except:

            return {
                'code': -1,
                'message': error_select

            }
    return {
        'code': -1,
        'message': error_connect
    }



