import psycopg2
from config import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback
from data_base.answer_db import error_connect,\
    error_select,\
    step_to_log_added


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def get_id_user_by_chat_id(chat_id, cursor):
    select = '''
        SELECT id_user
        FROM users
        WHERE chat_id = '{}'
    '''.format(chat_id)
    cursor.execute(select)
    return cursor.fetchone()[0]


def registration_user(first_name, last_name, chat_id):
    conn = create_connection()
    if conn:
        try:
            select = '''
            INSERT INTO users (id_user, first_name, last_name, chat_id, created_on)
            VALUES (DEFAULT , '{}', '{}', '{}', now());
            '''.format(first_name, last_name, chat_id)
            cur = conn.cursor()
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return ''
        except:
            traceback.print_exc()
            return ''
    return ""
